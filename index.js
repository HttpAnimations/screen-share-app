// index.js
const http = require('http');
const fs = require('fs');
const WebSocket = require('ws');
const capture = require('desktop-capture');

// Create HTTP server
const server = http.createServer((req, res) => {
    // Serve HTML file
    fs.readFile('./index.html', (err, data) => {
        if (err) {
            res.writeHead(500);
            return res.end('Error loading index.html');
        }
        res.writeHead(200);
        res.end(data);
    });
});

// Create WebSocket server
const wss = new WebSocket.Server({ server });

// WebSocket connection handling
wss.on('connection', (ws) => {
    console.log('Client connected');

    // Dummy screen capture (replace with actual capture logic)
    const dummyStream = "dummy screen stream"; // Replace with actual screen capture stream

    // Broadcast captured stream to connected clients
    ws.send(dummyStream);

    // Handle client disconnect
    ws.on('close', () => {
        console.log('Client disconnected');
        // Stop screen capture or handle accordingly
    });
});

// Start server on port 9157
const PORT = 9157;
server.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`);
});
